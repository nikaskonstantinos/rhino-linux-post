---
Metadata
Title: Πως να εγκαταστήσετε το Rhino-linux στο Raspberry Pi 4B. Πλήρης οδηγός.
Alternative Title: How to install Rhino-linux on Raspberry Pi 4B. Beginner's guide.
Contributor: @nikaskonstantinos
Language: el
Date Issued: 2024.01.30
Publisher: Linux User Forum – Το forum των Ελλήνων χρηστών Linux – https://linux-user.gr/
Description: How to install Rhino-linux on Raspberry Pi 4B. Complete guide.
Category: Άρθρα και οδηγοί
Tags: rhino-linux, raspberrypi.

---



## Πως να εγκαταστήσετε το Rhino-linux στο Raspberry Pi 4B. Πλήρης οδηγός.

[toc]



### Ενότητα 1. Λήψη του αρχείου εγκατάστασης του Rhino-Linux.

---

#### 1.1. Εισαγωγή 

Το Rhino-Linux είναι μια διανομή βασισμένη στην Ubuntu αλλά με στόχο τη διαρκή ανανέωση σε συνδιασμό με τη χρήση ενός σύγχρονου περιβάλλοντος εργασίας με την ονομασία Unicorn βασισμένο στο Xfce και ενός εύχρηστου και ιδιαίτερου διαχειριστή πακέτων του rhino-pkg ή rpk βασισμένον στον pacstall.

Διατίθεται για αρχιτεκτονική υπολογιστών amd 64  ή arm 64, που χρησιμοποιείται στον οδηγό για την εγκατάσταση στο Raspberry Pi 4B.

#### 1.2. Λήψη του αρχείου εγκατάστασης του Rhino-Linux.

Στην ηλεκτρονική διεύθυνση <https://rhinolinux.org/download.html>   επιλέγουμε την έκδοση του αρχείου εγκατάστασης που επιθυμούμε, generic iso amd 64, ή arm 64 για pine 64(pinetab, pinephone) ή για raspberry pi καθώς και αν επιθυμούμε επιφάνεια εργασίας ή μόνο ένα αρχείο εγκατάστασης για διακομιστή χωρίς γραφικό περιβάλλον παρά μόνο με διεπαφή γραμμής εντολών. Στον συγκεκριμένο οδηγό επιλέγουμε την εικόνα του αρχείου εγκατάστασης για υπολογιστές με γραφικό περιβάλλον για το Rpi4.

Τα προκαθορισμένα διαπιστευτήρια είναι τα παρακάτω:

 όνομα χρήστη: ``rhino``  

κωδικός :``1234 ``

τα οποία επιβάλλεται για λόγους ασφαλείας να αλλάξουμε μετά την εγκατάσταση.

![ISO arm 64 - Raspberry Pi.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o1rhino2024-01-27_09-26.png?ref_type=heads)



Έπειτα επιλέγουμε το αποθετήριο από το οποίο θα κάνουμε τη λήψη του αρχείου ISO.

Είτε το αποθετήριο του Rhino-Linux, είτε αυτό του sourceforge.net, που επιλέξαμε.

![Αποθετήριο του sourceforge.net για το Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o2rhino2024-01-27_09-27.png?ref_type=heads)





![Λήψη του Rhino Linux από το sourceforge.net.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o3rhino2024-01-27_09-28.png?ref_type=heads)



Περιμένουμε λίγα λεπτά αναλόγως της ταχύτητας του δικτύου μας για την ολοκλήρωση της λήψης του αρχείου μεγέθους 1,7 GB στον υπολογιστή μας.

![Ολοκλήρωση της διαδικασίας λήψης του αρχείου .ISO εγκατάστασης.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o5rhino2024-01-27_09-48.png?ref_type=heads)



### Ενότητα 2. Εγκατάσταση του αρχείου .ISO με το πρόγραμμα RPI-IMAGER στο μέσο USB εγκατάστασης.

---

#### 2.1. Διαμόρφωση του USB για την εγκατάσταση του Rhino Linux στο Raspberry pi.

Σε έναν υπολογιστή εγκαθιστούμε το πρόγραμμα διαμόρφωσης του μέσου USB, ή SSD, ή USB με micro SDcard, ή micro SD card που θα χρησιμοποιήσουμε για το Rhino Linux. Επιλέξαμε το πρόγραμμα Imager που μπορούμε να εγκαταστήσουμε με την εντολή ``sudo apt install rpi-imager`` σε έναν υπολογιστή βασισμένο σε Ubuntu/Debian ή με το flatpak για όσες διανομές το υποστηρίζουν από τη διεύθυνση του flathub <https://flathub.org/apps/org.raspberrypi.rpi-imager>.

#### 2.2. Πρώτο βήμα: επιλογή του αρχείο Rhino Linux στο Imager.

Ανοίγουμε την εφαρμογή Imager στον υπολογιστή μας στον οποίο έχουμε τοποθετήσει και το κενό USB που θα χρησιμοποιήσουμε για την εγκατάσταση.

Επιλέγουμε το πρώτο πλήκτρο ``CHOOSE OS`` για την επιλογή του επιθυμητού λειτουργικού συστήματος και διανομής.

![Επιλογή λειτουργικού συστήματος στο Imager.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o7rhino2024-01-27_09-49.png?ref_type=heads)



Με κύλιση προς τα κάτω επιλέγουμε το πλήκτρο ``Use custom``.

![Προσαρμοσμένη επιλογή εικόνας .ISO.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o8rhino2024-01-27_09-50.png?ref_type=heads)



Κατευθυνόμαστε αυτόματα στο πρόγραμμα περιήγησης των αρχείων του υπολογιστή μας και επιλέγουμε το αρχείο με κατάληξη .ISO του Rhino Linux.

![Επιλογή του επιθυμητού αρχείου εγκατάστασης.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o9rhino2024-01-27_09-51.png?ref_type=heads)



Έπειτα επιλέγουμε το πλήκτρο ``Open`` στο πρόγραμμα περιήγησης των αρχείων μας για να ολοκληρωθεί η επιλογή.

![Ολοκλήρωση της επιλογής του αρχείου του Rhino Linux στο Imager.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o10rhino2024-01-27_09-52.png?ref_type=heads)



#### 2.3. Δεύτερο βήμα: επιλογή του χώρου εγκατάστασης του Rhino Linux στο Imager.

Στο πρόγραμμα Imager του υπολογιστή μας επιλέγουμε το δεύτερο πλήκτρο ``CHOOSE STORAGE`` για την επιλογή του μέσου αποθήκευσης του αρχείου εγκατάστασης του Rhino Linux.

![Επιλογή του μέσου αποθήκευσης του αρχείου εγκατάστασης του Rhino Linux στο Imager.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o11rhino2024-01-27_09-55.png?ref_type=heads)



Έπειτα μεταφέρουμε στο μέσο (USB με micro SDcard)  που έχουμε τοποθετήσει στη θύρα USB του υπολογιστή μας το αρχείο με κατάληξη .ISO του Rhino linux. Με το  USB αυτό θα γίνει η εγκατάσταση στο Raspberry Pi. Είναι προτιμότερο να χρησιμοποιήσουμε έναν δίσκο ssd, στην περίπτωση όμως που χρησιμοποιήσουμε USB επιλέγουμε να έχει τέτοια τεχνικά χαρακτηριστικά και χωρητικότητα που θα κάνουν αποδοτική τη χρήση του λειτουργικού μας συστήματος.

![USB με SD card 256 GB μέσο αποθήκευσης του αρχείου εγκατάστασης του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o12rhino2024-01-27_09-56.png?ref_type=heads)



#### 2.4. Τρίτο βήμα: εγγραφή του USB με το αρχείο εγκατάστασης του Rhino Linux στο Imager.

Τέλος επιλέγουμε στο πρόγραμμα Imager στον υπολογιστή μας το τρίτο πλήκτρο ``WRITE`` για την εγγραφή του USB με το αρχείο εγκατάστασης του Rhino Linux.

![Επιλογή εγγραφής του USB με το αρχείο εγκατάστασης του Rhino Linux στο Imager.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o13rhino2024-01-27_09-57.png?ref_type=heads)



Λαμβάνουμε δύο φορές προειδοποιητικά μηνύματα για το ότι αν υπάρχουν αρχεία στο USB αυτά θα διαγραφούν.

![Προειδοποιητικά μηνύματα του Imager.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o14rhino2024-01-27_09-57.png?ref_type=heads)

Εκόνα 14. Προειδοποιητικά μηνύματα του Imager.

Αφού επιλέξουμε το πλήκτρο ``YES`` ξεκινά η διαδικασία της εγγραφής  από τον υπολογιστή μας.

![Επιλογή εγγραφής του USB με το αρχείο εγκατάστασης του Rhino Linux στο Imager.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o15rhino2024-01-27_09-58.png?ref_type=heads)



Ακολουθεί αυτόματα ή επαλήθευση της εγγραφής.

![Επαλήθευση εγγραφής του USB με το αρχείο εγκατάστασης του Rhino Linux στο Imager.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o16rhino2024-01-27_10-02.png?ref_type=heads)

Τέλος για να ολοκληρωθεί η διαδικασία της εγγραφής απομακρύνουμε το USB  από τον υπολογιστή μας πατώντας και το πλήκτρο ``CONTINUE`` στο πρόγραμμα Imager.

![Ολοκλήρωση της διαμόρφωσης του USB και αποσύνδεση του από τον υπολογιστή.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o17rhino2024-01-27_10-05.png?ref_type=heads)



### Ενότητα 3. Εγκατάσταση του Rhino-Linux στο Raspberry pi.
---

#### 3.1. Οδηγός εγκατάστασης του Rhino-Linux.

Οι δημιουργοί του Rhino-Linux, υποθέτουμε ότι έχοντας σαν όραμα τη δημιουργία ενός ισχυρού και ταχύτατου λειτουργικού συστήματος, χρησιμοποίησαν ως λογότυπο την εικόνα ενός ρινόκερου.

Τοποθετούμε το USB που περιέχει το Rhino-Linux σε μία θύρα USB του Raspberry pi(Rpi), και στις υπόλοιπες θύρες ένα πληκτρολόγιο και ένα ποντίκι. Με ένα καλώδιο με αρσενικό micro HDMI, όχι mini HDMI από τη μία άκρη και αρσενικό HDMI από την άλλη άκρη, συνδέουμε το Rpi με μία οθόνη ή τηλεόραση σε λειτουργία HDMI.

Τέλος τροφοδοτούμε το Rpi συνδέοντας το στην πρίζα με τον φορτιστή του.

Σε επιτραπέζιο υπολογιστή η εγκατάσταση του Rhino Linux γίνεται με το πρόγραμμα Calamares.

Στο Rpi, απλά κάνουμε υπομονή αρκετά λεπτά και περιμένουμε την εμφάνιση της κονσόλας του Rhino Linux.

Έπειτα παρακαλοθούμε την εγκατάσταση του λειτουργικού συστήατος χωρίς να κάνουμε κάτι και μόλις ολοκληρωθεί βλεπουμε την παρακάτω οθόνη με τον οδηγό εγκατάστασης του Rhino Linux.

![Οδηγός εγκατάστασης του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o18rhino2024-01-27_10-45-20.png?ref_type=heads)

#### 3.2. Επιλογή σκοτεινού ή ανοικτού θέματος στο Rhino Linux.

Επιλέγουμε το μωβ πλήκτρο ``Let's Start`` της προηγούμενης εικόνας, έπειτα επιλέγουμε την εμφάνιση με σκοτενό ή φωτεινό θέμα που επιθυμούμε και πατάμε το πλήκτρο ``Next``.

![Επιλογή σκοτεινού ή ανοικτού θέματος στο Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o19rhino2024-01-27_10-46-32.png?ref_type=heads)



#### 3.3. Επιλογή προγραμμάτων εγκατάστασης εφαρμογών στο Rhino Linux.

Στη συνέχεια μετά τις επιλογές εμφάνισης ακολουθεί η επιλογή των προγραμμάτων εγκατάστασης εφαρμογών.

Μπορούμε να επιλέξουμε μεταξύ των Flatpak, Flatpak beta channel, Snap, Appimage και έτσι η εγκατάσταση τους γίνεται πολύ εύκολα από ένα απλό παράθυρο της διεπαφής του χρήστη, ιδίως για αρχάριους χρήστες.

Εμείς επιλέξαμε αρχικά, Flatpak και Appimage.

![Επιλογή προγραμμάτων εγκατάστασης εφαρμογών στο Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o20rhino2024-01-27_10-48-26.png?ref_type=heads)



#### 3.4. Επιλογή λοιπών διαχειριστών πακέτων και προγραμμάτων γραμμής εντολών.

Ο διαχειριστής πακέτων με την ονομασία ``Νala`` είναι προεπιλεγμένος και βασίζεται στη διαχείριση πακέτων του ``apt`` με πιο σύγχρονη μέθοδο και εμφάνιση. Μπορούμε όμως να τον απενεργοποιήσουμε εάν δεν τον επιθυμούμε. Επίσης για τους πιο έμπειρους χρήστες υπάρχει καιη επιλογή του ``Github CLI``.

![Επιλογή λοιπών διαχειριστών πακέτων και προγραμμάτων γραμμής εντολών.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o21rhino2024-01-27_10-49-03.png?ref_type=heads)



Για την εγκατάσταση βέβαια απαιτούνται δικαιώματα διαχειριστή του χρήστη ``rhino`` με κωδικό password ``1234`` όπως έχουμε αναφέρει.

![Έλεγχος  ταυτότητας στο Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o22rhino2024-01-27_10-49-31.png?ref_type=heads)



Μετά την ολοκλήρωση του ελέγχου ταυτότητας στο Rhino Linux απαιτείται μια επανεκκίνηση από το πλήκτρο ``Reboot Now``.

![Oλοκλήρωση του ελέγχου ταυτότητας στο Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o23rhino2024-01-27_10-52-10.png?ref_type=heads)



#### 3.5. Εφαρμογή πληροφοριών και διαχείρισης του συστήματος Rhino Linux.

Ένα άλλο εργαλείο του Rhino Linux, πολύ χρήσιμο σε αρχάριους χρήστες, είναι η εφαρμογή με γραφικό περιβάλλον στην οποία παρουσιάζονται οι πληροφορίες του συστήματος με την ονομασία ``Your System``

![Η εφαρμογή ``Your System``του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o24rhino2024-01-27_10-54-28.png?ref_type=heads)



Από το πλήκτρο ``System Upgrade``μπορούμε με μια κίνηση να κάνουμε αναβάθμιση του συστήματός μας.

![Αναβάθμιση του συστήματος Rhino Linux  από την εφαρμογή ``Your System``.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o25rhino2024-01-27_10-54-55.png?ref_type=heads)



Από ότι παρατηρούμε αρχικά χρησιμοποείται ο διαχειριστής πακέτων ``nala``και στη συνέχεια ο διαχειριστής πακέτων ``pacstall``.

![Διαδικασία αναβάθμισης του συστήματος Rhino Linux  με την εφαρμογή ``Your System``.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o26rhino2024-01-27_10-55-54.png?ref_type=heads)



Στη συνέχεια επιλέγουμε τη γλώσσα του πληκτρολογίου μας. Για να περιηγηθούμε στα παράθυρα  διαλόγου ``ncurces`` που εμφανίζονται, χρησιμοποιούμε το πλήκτρο ``Tab``για μετακίνηση και τα πλήκτρα ``Space`` ή ``Enter`` για επαλήθευση.

![Επιλογή της γλώσσας του πληκτρολογίου.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o27rhino2024-01-27_10-58-32.png?ref_type=heads)



Η διαδικασία αυτή περιλαμβάνει ερωτήσεις για τη χώρα και τη γλώσσα.

![Επιλογή διάλεκτου γλώσσας.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o28rhino2024-01-27_10-59-02.png?ref_type=heads)



Επίσης έχουμε τη δυνατότητα να επιλέξουμε και τη διάταξη του πληκτρολογίου.

![Επιλογή διάταξης πληκτρολογίου.

](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o29rhino2024-01-27_10-59-41.png?ref_type=heads)



Ακολουθεί η επιλογή τοης γεωγραφικής περιοχής όπου κατοικεί ο χρήστης.

![Επιλογή γεωγραφικής περιοχής.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o30rhino2024-01-27_11-00-11.png?ref_type=heads)



Η επιλογή της γωγραφικής περιοχής περιλαμβάνει την ήπειρο.

![Επιλογή ηπείρου.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o31rhino2024-01-27_11-00-44.png?ref_type=heads)



Τέλος η επιλογή της γωγραφικής περιοχής περιλαμβάνει και την πόλη.

![Επιλογή πόλης.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o32rhino2024-01-27_11-01-27.png?ref_type=heads)



Η ερώτηση για τη γεωγραφική περιοχή και τη γλώσσα επαναλαμβάνεται περίπου δύο φορές και μετά ολοκληρώνεται η διαδικασία αναβάθμισης του Rhino Linux.

![Ολοκλήρωση της αναβάθμισης στο Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o33rhino2024-01-27_13-25-24.png?ref_type=heads)



#### 3.6. Ο διαχειριστής πακέτων ``rhino-pkg`` ή αλλιώς ``rpk`` του Rhino Linux.

Μία από τις σπουδαιότερες καινοτομίες του Rhino Linux είναι ο διαχειριστής εγκατάστασης πακέτων εφαρμογών ο οποίος ονομάζεται  ``rhino-pkg`` ή αλλιώς ``rpk`` . Βασίζεται στον ``Pacstall``. 

Για την ενημέρωση των εφαρμογών, είτε έχουν εγκατασταθεί με flatpak, snap, appimage, apt, nala ή git clone, αρκεί η εντολή: 

``rhino-pkg update -y`` ή αλλιώς ``rpk update -y``.

Για την απομάκρυνση των πακέτων που δεν χρησιμοποιούνται πλέον υπάρχει η εντολή:

``rpk cleanup``.

![Ο διαχειριστής πακέτων ``rhino-pkg`` ή αλλιώς ``rpk`` του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o35rhino2024-01-27_13-33-15.png?ref_type=heads)


Το πρόγραμμα neofetch έρχεται προεγκατστημένο.

![Tο πρόγραμμα neofetch στον Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o36rhino2024-01-27_13-34-37.png?ref_type=heads)



Εάν θέλουμε να αναζητήσουμε μια εφαρμογή για εγκατάσταση στον Rhino Linux, για παράδειγμα την εφαρμογή flameshot, χρησιμοποιούμε την παρακάτω εντολή από τον εξομοιωτή τερματικού:

``rpk search flameshot`` 

και εμφανίζονται πληροφορίες αν μπορεί να εγκατασταθεί με apt, flatpak ή άλλο τρόπο που έχουμε εγκατεστημένο στο σύστημά μας.

![Αναζήτηση εφαρμογών με τον διαχειριστή πακέτων ``rpk`` του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o37rhino2024-01-27_13-36-09.png?ref_type=heads)



Η εγκατάσταση των εφαρμογών, για παράδειγμα της εφαρμογής flameshot που αναζητήσαμε γίνεται με την εντολή:

``rpk install flameshot``.

Eπιλέγουμε επίσης ποιά μορφή πακέτου επιθυμούμε για παράδειγμα apt ή flatpak.

![Εγκατάσταση εφαρμογών με τον διαχειριστή πακέτων ``rpk`` του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o38rhino2024-01-27_13-37-56.png?ref_type=heads)



#### 3.7. Επιλογή πρόσθετης γλώσσας στο Rhino Linux.

Για την εγκατάσταση του συστήματος στο Rpi είχαμε επιλέξει την αγγλική γλώσσα. Για να προσθέσουμε την ελληνική γλώσσα στο πληκτρολόγιο, από το πλέγμα των εφαρμογών του Rhino Linux, επιλέγουμε την εφαρμογή ``Keyboard`` απενεργοποιούμε το πλήκτρο ``Use system defaults`` που βρίσκεται πάνω δεξιά θέση και έπειτα στην κάτω δεξιά θέση επιλέγουμε το πλήκτρο ``Add``.

![Επιλογή πρόσθετης γλώσσας στο Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o39rhino2024-01-27_13-41.png?ref_type=heads)



Από το κυλιόμενο μενού που εμφανίζεται επιλέγουμε την ελληνική γλώσσα και έπειτα πατάμε το πλήκτρο ``Οk``.

![Επιλογή ελληνικής γλώσσας στο Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o40rhino2024-01-27_13-42.png?ref_type=heads)


#### 3.8. Το γραφικό περιβάλλον του Rhino Linux.

Μία άλλη καινοτομία θα λέγαμε του Rhino Linux είναι το περιβάλλον εργασίας που διαθέτει, δηλαδή το Desktop Environment με την ονομασία ``Unicorn`` το οποίο είναι ουσιαστικά ένα παραμετροποιημένο Xfce.

Στην εικόνα που ακολουθεί φαίνεται ο πίνακας ελέγχου του γραφικού περιβάλλοντος του Rhino Linux.

![Πίνακας ελέγχου του γραφικού περιβάλλοντος του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o41rhino2024-01-27_13-45-11.png?ref_type=heads)



Το γραφικό περιβάλλον του Rhino Linux, διαθέτει επίσης τον ``Cortile`` για αυτόματη τακτοποίηση των παραθύρων στην επιφάνεια εργασίας, μια λειτουργία (auto tiling), που θυμίζει αυτή του λειτουργικού συστήματος Pop_!Os.

![Λειτουργία αυτόματης τακτοποίησης των παραθύρων στην επιφάνεια εργασίας του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o43rhino2024-01-27_13-57.png?ref_type=heads)



#### 3.9. Η εφαρμογή ``RhinoDrop``του Rhino Linux.

Μία άλλη εφαρμογή που αξίζει να αναφερθεί, είναι η εφαρμογή ``RhinoDrop`` του Rhino Linux, που βασίζεται στην εφαρμογή ``Snapdrop``  και μπορεί να χρησιμοποιθεί για μεταφορά αρχείων στο τοπικό δίκτυο του Rhino Linux, ανεξαρτήτως λειτουργικού συστήματος από τη διεύθυνση:

<https://drop.rhinolinux.org/>

![Η εφαρμογή ``RhinoDrop``του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o44rhino2024-01-27_14-00.png?ref_type=heads)



Στην παρακάτω εικόνα βλέπουμε πως εμφανίζεται ένας υπολογιστής στην εφαρμογή ``RhinoDrop``του Rhino Linux.

![Διαμοιρασμός αρχείων με την εφαρμογή ``RhinoDrop``του Rhino Linux.](https://gitlab.com/nikaskonstantinos/rhino-linux-post/-/raw/main/images/o45rhino2024-01-27_14-02.png?ref_type=heads)


#### 3.10. Σύνοψη.

To Rhino Linux είναι μια διανομή βασισμένη στην Ubuntu και απευθύνεται σε ένα ευρύ κοινό χρηστών. Κύρια χαρακτηριστικά της είναι η ευκολία χρήσης και εγκατάστασης νέων εφαρμογών χάρη στον διαχειριστή πακέτων ``rpk`` που διαθέτει, καθώς και η σύγχρονη εμφάνισή του χάρη στο ``Xfce``  και το ``Unicorn``.

Περισσότερες πληροφορίες θα βρείτε στην επίσημη ιστοσελίδα του Rhino Linux που φιλοξενείται στη διεύθυνση:

<https://rhinolinux.org/>
 

 Ο παραπάνω οδηγός διατίθεται και με άδεια Creative Commons Αναφορά 4.0 στην παρακάτω διεύθυνση:
 <https://gitlab.com/nikaskonstantinos/rhino-linux-post>
